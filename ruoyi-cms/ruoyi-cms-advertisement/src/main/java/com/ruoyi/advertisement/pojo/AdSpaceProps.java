package com.ruoyi.advertisement.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * 广告位自定义属性
 * 
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
@Getter
@Setter
public class AdSpaceProps {

	/**
	 * 广告策略
	 */
	private String strategy;
}
