package com.ruoyi.contentcore.perms;

public interface ContentCorePriv {

	public String SiteView = "cms:site:view";

	public String CatalogView = "cms:content:view";

	public String ContentView = "cms:content:view";

	public String ResourceView = "cms:resource:view";

	public String PublishPipeView = "cms:publishpipe:view";

	public String TemplateView = "cms:template:view";

	public String FileView = "cms:file:view";

	public String StaticizeView = "cms:staticize:view";

	public String EXModelView = "cms:exmodel:view";
}
