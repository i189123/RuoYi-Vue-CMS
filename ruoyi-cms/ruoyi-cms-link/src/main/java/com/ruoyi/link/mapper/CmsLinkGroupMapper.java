package com.ruoyi.link.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.link.domain.CmsLinkGroup;

/**
 * <p>
 * 友链分组Mapper 接口
 * </p>
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface CmsLinkGroupMapper extends BaseMapper<CmsLinkGroup> {

}
