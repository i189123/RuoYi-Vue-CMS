package com.ruoyi.stat;

/**
 * 统计菜单
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public record StatMenu(String menuId, String parentId, String name, int sort) {

}
