package com.ruoyi.comment;

public interface CommentConsts {
    
    /**
     * 评论删除标识
     */
    public int DELETE_FLAG = 1;
}
