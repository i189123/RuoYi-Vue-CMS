package com.ruoyi.comment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.comment.domain.CommentLike;

/**
 * <p>
 * 评论点赞记录Mapper 接口
 * </p>
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface CommentLikeMapper extends BaseMapper<CommentLike> {

}