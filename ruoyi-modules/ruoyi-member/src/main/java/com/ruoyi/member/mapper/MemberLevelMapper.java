package com.ruoyi.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.member.domain.MemberLevel;

/**
 * <p>
 * 会员等级经验数据表Mapper 接口
 * </p>
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface MemberLevelMapper extends BaseMapper<MemberLevel> {

}