package com.ruoyi.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.member.domain.Member;

/**
 * <p>
 * 会员表Mapper 接口
 * </p>
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface MemberMapper extends BaseMapper<Member> {

}