package com.ruoyi.member.domain.vo;

/**
 * 会员等级数据VO
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public record MemberLevelVO(String levelType, Integer level, Long exp) {
}
