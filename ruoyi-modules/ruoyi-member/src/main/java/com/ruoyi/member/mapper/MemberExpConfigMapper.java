package com.ruoyi.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.member.domain.MemberExpConfig;

/**
 * 等级经验值配置Mapper 接口
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface MemberExpConfigMapper extends BaseMapper<MemberExpConfig> {

}