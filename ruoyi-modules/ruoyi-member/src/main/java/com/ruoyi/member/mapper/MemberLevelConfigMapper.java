package com.ruoyi.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.member.domain.MemberLevelConfig;

/**
 * 等级配置Mapper 接口
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface MemberLevelConfigMapper extends BaseMapper<MemberLevelConfig> {

}