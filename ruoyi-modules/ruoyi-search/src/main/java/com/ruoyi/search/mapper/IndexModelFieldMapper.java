package com.ruoyi.search.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.search.domain.IndexModelField;

/**
 * <p>
 * 搜索模型字段Mapper 接口
 * </p>
 *
 * @author 兮玥
 * @email liweiyimwz@126.com
 */
public interface IndexModelFieldMapper extends BaseMapper<IndexModelField> {

}